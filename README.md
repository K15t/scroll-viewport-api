# Description

This repository contains the public Java API for [Scroll Viewport](https://marketplace.atlassian.com/plugins/com.k15t.scroll.scroll-viewport)

Binary, Javadoc and source artifacts are published through our Maven repository. See below for instructions how to access them.

The API project itself uses [semantic versioning](https://semver.org), Scroll Viewport however do not.

# License
The Scroll Viewport public APIs are licensed under the terms of the *MIT license*.

# Usage

The public APIs can be used by adding a `provided` Maven dependency to the project. At runtime this will be provided by the *Scroll Viewport* app.

First you'll need to add the K15t Maven repository to your project or Maven settings:
```xml
<repositories>
    <repository>
        <id>k15t</id>
        <url>https://nexus.k15t.com/content/repositories/releases</url>
    </repository>
</repositories>
```

Then add this dependency in `pom.xml`:
```xml
<dependencies>
    ...

    <dependency>
        <groupId>com.k15t.scroll</groupId>
        <artifactId>scroll-viewport-api</artifactId>
        <version>1.0.0</version>
        <scope>provided</scope>
    </dependency>

    ...
</dependencies>
```

Finally add the following OSGi imports to your `maven-confluence-plugin` configuration in `pom.xml`:
```xml
<plugin>
    <groupId>com.atlassian.maven.plugins</groupId>
    <artifactId>maven-confluence-plugin</artifactId>
    <configuration>
        ...
        <instructions>
            <Import-Package>
                com.k15t.scroll.viewport.api.*;version="[1.0.0,2.0.0)",
                ...
            </Import-Package>
            ...        
        </instructions>
        ...
    </configuration>
</plugin>
```
The version range `[1.0.0,2.0.0)` declares that your plugin is compatible with all versions of the API starting from (and including) version 1.0.0 up to (but NOT including) 2.0.0.

A new major version of the API will have breaking changes, so it is very probable that you'll need to adapt your source code. Therefore you should not assume that your code will work out of the box with the next major version of the API.

We recommend to declare the version range as follows:

* Use the version of the maven dependency as the lower bound
* Use the next major version as the **excluded** upper bound

## API Usage Examples
```java
public class ApiExamples {

    @ComponentImport @Autowired private ViewportService viewportService;
    @ComponentImport @Autowired private ThemeService themeService;
    @ComponentImport @Autowired private ViewportSpacePermissionService viewportSpacePermissionService;


    public URI createViewportFromScratch(String targetSpaceKey) {
        Theme theme = themeService.getAvailableThemes(targetSpaceKey).stream()
                .filter(candidateTheme -> candidateTheme.getName().equals("My Theme"))
                .findFirst()
                .orElseThrow(IllegalStateException::new);

        ViewportConfig config = viewportService.createNewViewportConfig()
                .withThemeId(theme.getId())
                .withPages(true)
                .withBlogPosts(false)
                .withVersions(false)
                .withVariants(false)
                .withTranslations(false)
                .withComalaWorkflows(false)
                .withDomainName("demo.k15t.com")
                .withViewportPrefix(targetSpaceKey.toLowerCase())
                .withPagePathNaming(ViewportConfig.PagePathNaming.HIERARCHICAL);

        Viewport viewport = viewportService.createViewport(targetSpaceKey, config);
        return viewport.getViewportUrl();
    }


    public URI copyViewportFromTemplateSpace(String targetSpaceKey) {
        Viewport templateViewport = viewportService.getViewport(null, "/template")
                .orElseThrow(() -> new IllegalArgumentException("No such viewport."));

        ViewportConfig newConfig = templateViewport.getConfig()
                .withViewportPrefix(targetSpaceKey.toLowerCase());

        Viewport newViewport = viewportService.createViewport(targetSpaceKey, newConfig);

        return newViewport.getViewportUrl();
    }


    public void setFlatPagePathNamingInExistingViewport(String viewportId) {
        Viewport existingViewport = viewportService.getViewport(viewportId)
                .orElseThrow(() -> new IllegalArgumentException("No such viewport."));

        ViewportConfig newConfig = existingViewport.getConfig()
                .withPagePathNaming(ViewportConfig.PagePathNaming.FLAT);

        viewportService.updateViewport(existingViewport.getId(), newConfig);
    }


    public void moveViewportToNewSpace(String viewportId, String targetSpaceKey) {
        ViewportConfig existingViewportConfig = viewportService.getViewport(viewportId)
                .orElseThrow(() -> new IllegalArgumentException("No such viewport."))
                .getConfig();

        viewportService.deleteViewport(viewportId);

        Viewport movedViewport = viewportService.createViewport(targetSpaceKey, existingViewportConfig);
    }


    public URI getViewportUrlForPage(AbstractPage pageOrBlogpost) {
        Viewport viewport = viewportService.getViewports(pageOrBlogpost.getSpaceKey()).stream()
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("No viewport in space."));

        return viewport.createUrl(pageOrBlogpost.getId());
    }


    public void restrictSpaceAccess(String spaceKey) {
        ViewportSpacePermissions spacePermissions = viewportSpacePermissionService.getSpacePermissions(spaceKey);
        ViewportSpacePermissions newPermissions = spacePermissions
                .withConfluenceUiAccessRestricted(true)
                .withPermittedGroups("super-users", "confluence-administrators")
                .withRedirectEnabled(true);
        viewportSpacePermissionService.saveSpacePermissions(spaceKey, newPermissions);
    }
}
```
