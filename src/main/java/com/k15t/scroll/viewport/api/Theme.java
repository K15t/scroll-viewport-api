package com.k15t.scroll.viewport.api;

import java.util.Optional;


/**
 * Represents a Scroll Viewport theme to be used in viewports. Themes can be global or scoped to a space. Global themes can be used by
 * viewports from all spaces. Themes scoped to a certain space can only be used in that space.
 */
public interface Theme {

    /**
     * @return A unique ID identifying this theme.
     */
    String getId();


    /**
     * @return The name of the theme as displayed in the user interface.
     */
    String getName();


    /**
     * @return An {@link Optional} containing the key of the space this theme is scoped to. Empty for global themes.
     */
    Optional<String> getSpaceKey();

}
