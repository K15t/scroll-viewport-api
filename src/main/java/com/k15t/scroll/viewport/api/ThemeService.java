package com.k15t.scroll.viewport.api;

import javax.annotation.Nullable;

import java.util.List;


/**
 * Provides access to Scroll Viewport themes.
 */
public interface ThemeService {

    /**
     * Returns themes available to the specified context.
     *
     * @param spaceKey Optional. If specified the returned list contains both global themes and themes scoped to the specified space.
     * Otherwise only global themes are returned.
     * @return A list of {@link Theme}s.
     */
    List<Theme> getAvailableThemes(@Nullable String spaceKey);

}
