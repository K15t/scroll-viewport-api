package com.k15t.scroll.viewport.api;

import java.net.URI;
import java.net.URLEncoder;
import java.util.Map;


/**
 * Represents a Viewport. Viewports always belong to a Confluence space. Multiple viewports can be set up for a single space.
 */
public interface Viewport {

    /**
     * @return A unique ID identifying this viewport.
     */
    String getId();


    /**
     * @return The key of the space this viewport belongs to.
     */
    String getSpaceKey();


    /**
     * @return An immutable {@link ViewportConfig} representing the configuration of this viewport.
     */
    ViewportConfig getConfig();


    /**
     * @return The base URL for this viewport.
     */
    URI getViewportUrl();


    /**
     * Creates a {@link URI} for the specified page within this viewport.
     * <p>
     * If the space that the referenced page belongs to has version / variant / translation management enabled the defaults for those
     * will be used. To create URIs for specific versions, variants and translations use {@link #createUrl(long, Map)} with the
     * respective repository property values.
     * </p>
     *
     * @param confluencePageId The ID of a page or blogpost to create the viewport URL for.
     * @return The viewport URL for the specified page or blog post.
     * @throws IllegalArgumentException If the specified page or blogpost does not exist or is not contained in the space this viewport
     * belongs to.
     */
    URI createUrl(long confluencePageId);


    /**
     * Creates a {@link URI} for the specified page and given repository property values within this viewport.
     * <p>
     * The supported set of repository properties depend on the features enabled in that space and in the viewport configuration:
     * </p>
     * <table>
     * <tr><th>Property Key</th><th>Property Value</th><th>Available</th></tr>
     * <tr><td>{@code scroll-versions:version-name}</td><td>A Scroll Versions version name.</td></tr>
     * <tr><td>{@code scroll-versions:variant-name}</td><td>A Scroll Versions variant name.</td></tr>
     * <tr>
     * <td>{@code scroll-translations:language-key}</td>
     * <td>A Scroll Translations language key, see {@link java.util.Locale#toString()} for potential values.</td>
     * </tr>
     * <tr>
     * <td>{@code comala-workflows:state}</td>
     * <td>Use {@code {published}} to display the last published content, omit for latest content.</td>
     * </tr>
     * </table>
     * Version and variant names as well as language keys must be pre-processed as follows (see the output of the respective Scroll
     * Viewport theme placeholders):
     * <ol>
     * <li>Replace whitespace and '/' characters with '-'</li>
     * <li>Convert to lower case</li>
     * <li>Perform URL encoding via {@link URLEncoder#encode(java.lang.String, java.lang.String)}</li>
     * </ol>
     *
     * @param confluencePageId The ID of a page or blogpost to create the viewport URL for.
     * @param propertyValues The property values of repository to create the viewport URL for.
     * @return The viewport URL for the specified page or blog post.
     * @throws IllegalArgumentException If the specified page or blogpost does not exist or is not contained in the space this viewport
     * belongs to.
     */
    URI createUrl(long confluencePageId, Map<String, String> propertyValues);

}
