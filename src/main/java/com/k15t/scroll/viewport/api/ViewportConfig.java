package com.k15t.scroll.viewport.api;

import javax.annotation.Nullable;

import java.util.Optional;


/**
 * Represents the configuration of a viewport. <p>Instances of this interface are typically immutable. Therefore one of the {@code with...}
 * methods must be used to change the configuration and the new configuration object needs to be saved using
 * {@link ViewportService#createViewport(String, ViewportConfig)} or {@link ViewportService#updateViewport(String, ViewportConfig)}.</p>
 */
public interface ViewportConfig {

    /**
     * Represents the page path naming strategies supported by Scroll Viewport. See the
     * <a href="https://help.k15t.com/scroll-viewport/latest/?contentKey=config-guide">documentation</a>
     * for further information.
     */
    enum PagePathNaming {
        HIERARCHICAL, FLAT
    }


    /**
     * @return The ID of the {@link Theme} used by this viewport.
     */
    String getThemeId();


    /**
     * @return If pages are visible in this viewport.
     */
    boolean isPagesEnabled();


    /**
     * @return If blog posts are visible in this viewport.
     */
    boolean isBlogPostsEnabled();


    /**
     * @return {@code true} if feedback is turned on in this viewport.
     * <p>If feedback is enabled for at least one viewport in a space, the feedback will be displayed in the Confluence UI.</p>
     * <p>See the <a href="https://help.k15t.com/scroll-viewport/latest/?contentKey=article-feedback">documentation</a>
     * for further information.</p>
     */
    boolean isFeedbackEnabled();


    /**
     * @return If the integration with versions is enabled. This requires
     * <a href="https://marketplace.atlassian.com/plugins/com.k15t.scroll.scroll-versions">Scroll Versions</a> to be installed.
     */
    boolean isVersionsIntegrationEnabled();


    /**
     * @return If the integration with variants is enabled. This requires
     * <a href="https://marketplace.atlassian.com/plugins/com.k15t.scroll.scroll-versions">Scroll Versions</a> to be installed.
     */
    boolean isVariantsIntegrationEnabled();


    /**
     * @return If the integration with translations is enabled. This requires
     * <a href="https://marketplace.atlassian.com/plugins/com.k15t.scroll.scroll-translations">Scroll Translations</a> to be installed.
     */
    boolean isTranslationsIntegrationEnabled();


    /**
     * @return If the integration with Comala Workflows is enabled. This requires both
     * <a href="https://marketplace.atlassian.com/plugins/com.comalatech.workflow.comala-workflows">Comala Workflows</a>
     * as well as the
     * <a href="https://marketplace.atlassian.com/plugins/k15t-comala-workflows-for-scroll-exporters">integration app</a> to be installed.
     */
    boolean isComalaWorkflowsIntegrationEnabled();


    /**
     * @return An {@link Optional} containing the domain name for which this viewport is responsible. If empty the viewport is applied to
     * all domains Confluence is reachable with. Otherwise it handles only requests received through the specified domain. Typically this is
     * an empty {@link Optional} if Confluence is only available via one domain - the one from the Confluence base URL.
     * <p>For any combination of viewport domain and prefix only one viewport can be configured.</p>
     * <p> See the
     * <a href="https://help.k15t.com/scroll-viewport/latest/?contentKey=custom-domains-and-reverse-proxies">documentation</a>
     * for further information.</p>
     * @see #getViewportPrefix()
     */
    Optional<String> getViewportDomainName();


    /**
     * @return The path prefix for this viewport. If set to / the viewport is applied to the Confluence base URL or to the custom
     * viewport domain.
     * <p>For any combination of viewport domain and prefix only one viewport can be configured.</p>
     * <p>See the
     * <a href="https://help.k15t.com/scroll-viewport/latest/?contentKey=config-guide">documentation</a> for further information.</p>
     * @see #getViewportDomainName()
     */
    String getViewportPrefix();


    /**
     * @return The {@link PagePathNaming} used to create the URL paths for pages.
     */
    PagePathNaming getPagePathNaming();


    /**
     * @return A copy of this viewport configuration with the specified theme ID.
     */
    ViewportConfig withThemeId(String id);


    /**
     * @return A copy of this viewport configuration with the specified setting for enabling or disabling pages in this viewport.
     */
    ViewportConfig withPages(boolean enable);


    /**
     * @return A copy of this viewport configuration with the specified setting for enabling or disabling blog posts in this viewport.
     */
    ViewportConfig withBlogPosts(boolean enable);


    /**
     * @return A copy of this viewport configuration with the specified setting for enabling or disabling the versions integration.
     */
    ViewportConfig withVersions(boolean enable);


    /**
     * @return A copy of this viewport configuration with the specified setting for enabling or disabling the variants integration.
     */
    ViewportConfig withVariants(boolean enable);


    /**
     * @return A copy of this viewport configuration with the specified setting for enabling or disabling the translations integration.
     */
    ViewportConfig withTranslations(boolean enable);


    /**
     * @return A copy of this viewport configuration with the specified setting for enabling or disabling the Comala Workflows integration.
     */
    ViewportConfig withComalaWorkflows(boolean enable);


    /**
     * @param domainName The domain name that will be used by the viewport. If set to {@code null}, the viewport can be accessed with any
     * domain that Confluence can be accessed with.
     * @return A copy of this viewport configuration with the specified domain name.
     */
    ViewportConfig withDomainName(@Nullable String domainName);


    /**
     * @return A copy of this viewport configuration with the specified viewport prefix.
     */
    ViewportConfig withViewportPrefix(String viewportPrefix);


    /**
     * @return A copy of this viewport configuration with the specified {@link PagePathNaming}.
     */
    ViewportConfig withPagePathNaming(ViewportConfig.PagePathNaming pagePathNaming);


    /**
     * @return A copy of this viewport configuration with the specified setting for enabling or disabling scripts execution in attachments.
     */
    ViewportConfig withUnsafeAttachmentsEnabled(boolean unsafeAttachmentsEnabled);

}
