package com.k15t.scroll.viewport.api;

import javax.annotation.Nullable;

import java.util.List;
import java.util.Optional;


/**
 * Provides access to viewports.
 */
public interface ViewportService {

    /**
     * Returns a list of viewports for the specified context.
     *
     * @param spaceKey Optional. If specified the returned list contains viewport from the specified space. Otherwise viewports from all
     * spaces are returned.
     * @return A list of {@link Viewport}s.
     */
    List<Viewport> getViewports(@Nullable String spaceKey);


    /**
     * @return An {@link Optional} containing the viewport for the specified ID or an empty {@link Optional} if no viewport could be
     * found for this ID.
     */
    Optional<Viewport> getViewport(String viewportId);


    /**
     * Retrieves the viewport matching the specified domain name and prefix if it exists.
     *
     * @param domainName Optional - The domain name used by the viewport to find. If not specified then only viewports without an
     * explicit domain name (those using the domain from the Confluence base URL) qualify.
     * @param viewportPrefix The prefix used by the viewport to find.
     * @return An {@link Optional} containing the viewport for the specified parameters or an empty {@link Optional} if no viewport could be
     * found.
     */
    Optional<Viewport> getViewport(@Nullable String domainName, String viewportPrefix);


    /**
     * Deletes the specified viewport. Does nothing if no viewport exists for the specified ID.
     */
    void deleteViewport(String viewportId);


    /**
     * @return A new {@link ViewportConfig} that can be modified and then used with {@link #createViewport(String, ViewportConfig)} or
     * {@link #updateViewport(String, ViewportConfig)}. All fields are empty / disabled in this configuration and must be set explicitly.
     */
    ViewportConfig createNewViewportConfig();


    /**
     * Creates a new viewport.
     *
     * @param spaceKey The key of the space to create the viewport in.
     * @param config The {@link ViewportConfig} to use for the new viewport.
     * @return The new {@link Viewport}.
     * @throws IllegalArgumentException If the viewport cannot be created because:<ul>
     * <li>the specified space does not exist,</li>
     * <li>the {@link Theme} specified in the configuration does not exist or is not available in the specified space,</li>
     * <li>the combination of domain name and viewport prefix specified in the configuration is already used by another viewport.</li>
     * </ul>
     */
    Viewport createViewport(String spaceKey, ViewportConfig config);


    /**
     * Updates the specified viewport with a new configuration.
     *
     * @param viewportId The ID of the viewport to update.
     * @param newConfig The new {@link ViewportConfig} to apply to the existing viewport.
     * @return The updated {@link Viewport}.
     * @throws IllegalArgumentException If the viewport cannot be created because:<ul>
     * <li>the specified viewport does not exist,</li>
     * <li>the {@link Theme} specified in the configuration does not exist or is not available in this viewport's space,</li>
     * <li>the combination of domain name and viewport prefix specified in the configuration is already used by another viewport.</li>
     * </ul>
     */
    Viewport updateViewport(String viewportId, ViewportConfig newConfig);

}
