package com.k15t.scroll.viewport.api;


/**
 * Provides access to viewport space level permissions.
 */
public interface ViewportSpacePermissionService {

    /**
     * @return The configured {@link ViewportSpacePermissions} for the specified space or a default configuration if no viewport permissions
     * have been configured for this space.
     * @throws IllegalArgumentException If the specified space does not exist.
     */
    ViewportSpacePermissions getSpacePermissions(String spaceKey);


    /**
     * Saves the specified permissions.
     *
     * @param spaceKey The key of the space to save the viewport space permissions configuration to.
     * @param newSpacePermissions The new {@link ViewportSpacePermissions} to be saved.
     * @throws IllegalArgumentException If the specified permissions contain invalid group names or the specified space does not exist.
     */
    void saveSpacePermissions(String spaceKey, ViewportSpacePermissions newSpacePermissions);

}
