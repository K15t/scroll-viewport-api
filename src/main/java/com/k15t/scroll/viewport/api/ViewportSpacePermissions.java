package com.k15t.scroll.viewport.api;

import java.util.List;


/**
 * Represents the space level permissions for spaces with viewports. This can be used to control user access to the Confluence user
 * interface. Please note that users still need Confluence (space) permissions to view content. See the
 * <a href="https://help.k15t.com/scroll-viewport/latest/?contentKey=config-guide">documentation</a> for further information.
 */
public interface ViewportSpacePermissions {

    /**
     * @return If any restrictions should be applied to this space's Confluence UI access.
     * @see #getPermittedGroups()
     */
    boolean isConfluenceUiAccessRestricted();


    /**
     * @return A list of Confluence group names that are allowed to access the Confluence UI.
     */
    List<String> getPermittedGroups();


    /**
     * @return always {@code true}
     * @deprecated since Viewport 2.15.0 (API version 1.0.1) this option doesn't exist anymore and users are always redirected.
     */
    @Deprecated
    boolean isRedirectEnabled();


    /**
     * @return A copy of this permission configuration with the specified setting for enabling or disabling the UI access restriction.
     */
    ViewportSpacePermissions withConfluenceUiAccessRestricted(boolean restricted);


    /**
     * @return A copy of this permission configuration with the specified permitted group names.
     */
    ViewportSpacePermissions withPermittedGroups(String... permittedGroupNames);


    /**
     * @return A copy of this permission configuration.
     * @deprecated since Viewport 2.15.0 (API version 1.0.1) this option doesn't exist anymore and this method has no effect.
     */
    @Deprecated
    ViewportSpacePermissions withRedirectEnabled(boolean enabled);

}
